package dev.gilreath.springangularfront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAngularFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAngularFrontApplication.class, args);
	}

}
